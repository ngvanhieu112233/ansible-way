#!/bin/bash
file_new="/opt/zimbra/common/conf/logins_map.cf.new"
smtpd_sender_login_maps="/opt/zimbra/common/conf/logins_map.cf"
zmprov="/opt/zimbra/bin/zmprov"
#Make new file 
true > $file_new
$zmprov gad | grep -v "iwayvietnam.com" > /tmp/domainlist.txt
for domain in `cat /tmp/domainlist.txt`; do
echo "rewrite domain $domain"
echo "/.*@$domain/          admin@securemail.vn" >> $file_new
done
rsync -av $file_new $smtpd_sender_login_maps