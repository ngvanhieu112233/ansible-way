#!/bin/bash

inet_interfaces="inet_interfaces = 210.86.231.39, 127.0.0.1"

if grep -Fxq "$inet_interfaces" /opt/status_before_patch/`date +"%Y-%m-%d"`/inet_interfacesafter.txt
then
    echo "Fix patch not overwrite config inet_interfaces"
else
    echo "Fix patch overwrite config inet_interfaces, redo configture"
    postconf -e inet_interfaces="210.86.231.39, 127.0.0.1"
fi

smtp_bind_address="smtp_bind_address = 210.86.231.39"

if grep -Fxq "$smtp_bind_address" /opt/status_before_patch/`date +"%Y-%m-%d"`/smtp_bind_addressafter.txt
then
    echo "Fix patch not overwrite config smtp_bind_address"
else
    echo "Fix patch overwrite config smtp_bind_address, redo configture"
    postconf -e smtp_bind_address="210.86.231.39"
fi