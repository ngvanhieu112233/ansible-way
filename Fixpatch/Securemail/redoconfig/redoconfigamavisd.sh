#!/bin/bash

inet_acl="@inet_acl = qw( %%zimbraMtaMyNetworks%% ),"

if grep -Fxq "$inet_acl" /opt/zimbra/conf/amavisd.conf.in
then
    echo "Fix patch not overwrite file amavisd.conf.in"
else
    echo "Fix patch overwrite file amavisd.conf.in, insert whitelist ZimbraMtaMyNetWorks"
    sed -i  '/sub {0}],/a @inet_acl = qw( %%zimbraMtaMyNetworks%% ),' /opt/zimbra/conf/amavisd.conf.in
fi