# Quang Nam Zimbra Custom UI
## Installation
* Copy all contents to /opt/zimbra/jetty_base/webapps/zimbra
* Using text editor to open /opt/zimbra/jetty_base/etc/zimbra.web.xml.in
* Go to `param-name` **zimbraDefaultSkin** and set `param-value` to **quangnam**
* Restart mailboxd `zmmailboxdctl restart` under zimbra user.
